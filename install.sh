#!/bin/bash

clear

echo 'Starting the install for project:' $1;
echo 'Composer install';
composer update
echo 'Moving to theme folder';
cd web/app/themes/sp4
echo 'NPM install';
npm install
echo 'Bower install';
bower install
echo 'Gulp';
gulp
echo 'Renaming';
sed -i ".bak" "s/sp4/$1/g" style.css
rm style.css.bak
cd assets
sed -i ".bak" "s/sp4.dev/$1.dev/g" manifest.json
rm manifest.json.bak
cd ..
cd ..
mv sp4 $1
cd ..
cd ..
cd ..
echo 'Installing Wordpress';
wp core install --url=$1.dev --title=$1 --admin_user=sumedia --admin_password=P#D6zmtLaWHM7K6!Hx --admin_email=wordpress@sumedia.nl
echo 'Activating Plugins';
wp plugin activate --all
echo 'Activate Theme';
wp theme activate $1
echo 'Done';
