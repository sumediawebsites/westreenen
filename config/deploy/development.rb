set :stage, :staging

# Simple Role Syntax
# ==================
#role :app, %w{deploy@example.com}
#role :web, %w{deploy@example.com}
#role :db,  %w{deploy@example.com}

# Extended Server Syntax
# ======================
server "storage.sumedia.nl", user: "sumedia", port: "9990", roles: %w{web app db}
set :deploy_to, "/volume1/web/development/sp4"
set :wpcli_remote_url, 'development.sumedia.nl/sp4'
set :wpcli_local_url, 'sp4.dev'
set :keep_releases, 2
set :default_env, { path: "/usr/local/bin:$PATH" }
SSHKit.config.command_map[:composer] = "/usr/local/bin/composer"
set :wpcli_rsync_options, '-avz -e "ssh -p 9990"'
set :branch, :develop
set :log_level, :debug

# you can set custom ssh options
# it's possible to pass any option but you need to keep in mind that net/ssh understand limited list of options
# you can see them in [net/ssh documentation](http://net-ssh.github.io/net-ssh/classes/Net/SSH.html#method-c-start)
# set it globally
set :ssh_options, {
  auth_methods: %w(password),
  password: 'jBLnjezkS5IE!',
  user: 'sumedia',
}

fetch(:default_env).merge!(wp_env: :staging)
