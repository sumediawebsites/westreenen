set :stage, :production

# Simple Role Syntax
# ==================
#role :app, %w{deploy@example.com}
#role :web, %w{deploy@example.com}
#role :db,  %w{deploy@example.com}

# Extended Server Syntax
# ======================
server "5.61.250.156", user: "bosman", roles: %w{web app db}
set :deploy_to, "/home/bosman/domains/woneninloo.nl/public_html"
set :wpcli_remote_url, 'woneninloo.nl'
set :wpcli_local_url, 'westreenen.dev'
set :branch, :master

# you can set custom ssh options
# it's possible to pass any option but you need to keep in mind that net/ssh understand limited list of options
# you can see them in [net/ssh documentation](http://net-ssh.github.io/net-ssh/classes/Net/SSH.html#method-c-start)
# set it globally
set :ssh_options, {
  auth_methods: %w(password),
  password: '38eNKfNLta',
  user: 'bosman',
}

fetch(:default_env).merge!(wp_env: :production)