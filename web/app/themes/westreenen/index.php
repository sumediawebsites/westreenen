<?php
$context = Timber::get_context();

Timber::render('blog/archive.twig', $context, CACHE_LENGTH);
