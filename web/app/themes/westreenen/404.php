<?php
$context = Timber::get_context();

Timber::render('404.twig', $context, CACHE_LENGTH);
