<?php

$template          = '';
$context           = Timber::get_context();
$current_post_type = get_post_type();

// Referrer link setup
if ( wp_get_referer() ) {
	$context['prevUrl'] = wp_get_referer();
} else {
	$category = get_the_category();
	if ( ! empty( $category ) ) {
		$context['prevUrl'] = get_category_link( $category[0]->term_id );
	} else {
		$context['prevUrl'] = get_post_type_archive_link( $current_post_type );
	}
}

if (is_singular('kavels')) {

	$flexibles = get_field('pagina_opbouw', get_the_ID());

	if (!empty($flexibles)) {
		foreach ($flexibles as $flexible) {
			if ($flexible['acf_fc_layout'] == 'slider') {
				if ($flexible['scrolldown']) {
					$context['slider_title_exists'] = false;
				} else {
					$context['slider_title_exists'] = true;
				}
			}
		}
	}
	$context['kavelsliderstart'] = str_replace('Kavel ','', $post->post_title);

    $template = 'kavels/single.twig';
} else {
    $template = 'blog/single.twig';
}

Timber::render( $template, $context, CACHE_LENGTH );
