<?php
$context = Timber::get_context();

$flexibles = get_field('pagina_opbouw', get_the_ID());

if (!empty($flexibles)) {
    foreach ($flexibles as $flexible) {
        if ($flexible['acf_fc_layout'] == 'slider') {
            if ($flexible['scrolldown']) {
    	        $context['slider_title_exists'] = false;
            } else {
                $context['slider_title_exists'] = true;
            }
        }
    }
}

Timber::render('page.twig', $context, CACHE_LENGTH);
