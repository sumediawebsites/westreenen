<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
* Add <body> classes
*/
function body_class($classes) {
    // Add page slug if it doesn't exist
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    // Add class if sidebar is active
    if (Setup\display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
* Clean up the_excerpt()
*/
function excerpt_more() {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

/**
* Pre_get_posts
*/
function kavel_query($query) {

    if ($query->is_main_query() && !is_admin()) {
        if (is_post_type_archive('kavels')) {
            $query->set('posts_per_page', -1);
            $query->set('orderby', 'name');
            $query->set('order', 'ASC');

            return;
        }
    }

}

add_action('pre_get_posts', __NAMESPACE__ . '\\kavel_query');

/**
* Custom styles
*/
function my_mce_buttons_2( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}

add_filter( 'mce_buttons_2', __NAMESPACE__ . '\\my_mce_buttons_2' );

function my_mce_before_init_insert_formats( $init_array ) {
    $style_formats = array(
        array(
            'title'    => 'Intro',
            'selector' => 'p',
            'classes'  => 'intro',
            'wrapper'  => true
        ),
        array(
            'title'    => 'Button',
            'inline'   => 'span',
	        'selector' => 'a',
	        'classes'  => 'btn',
	        'wrapper'  => true
        ),
        array(
            'title'    => 'Button wit',
            'inline'   => 'span',
	        'selector' => 'a',
	        'classes'  => 'btn btn--white',
	        'wrapper'  => true
        ),
    );

    $init_array['style_formats'] = json_encode( $style_formats );
    return $init_array;
}

add_filter( 'tiny_mce_before_init', __NAMESPACE__ . '\\my_mce_before_init_insert_formats' );

/**
* Wrap div around post images
*/
function filter_ptags_on_images($content) {
    return preg_replace('/<p[^>]*>\\s*?(<a .*?><img.*?><\\/a>|<img.*?>)?\\s*<\/p>/', '<p class="border-holder">$1</p>', $content);
}
add_filter('the_content', __NAMESPACE__ . '\\filter_ptags_on_images');
add_filter('acf_the_content', __NAMESPACE__ . '\\filter_ptags_on_images');

/**
* Optimize and style Contact Form 7
*/
function deregister_cf7_js() {

    if (!is_admin() && !is_404()) {
    	$flexibles = get_post_meta(get_the_ID(), 'pagina_opbouw', false);

        if (isset($flexibles) && !empty($flexibles)) {
            foreach ($flexibles as $flexible) {
                if (!empty($flexible)) {
                    if (!in_array('contactformulier', $flexible)) {
                        wp_dequeue_script('contact-form-7');
                    }
                }
            }
        }
    }

}
add_action( 'wp_print_scripts', __NAMESPACE__ . '\\deregister_cf7_js' );

function deregister_ct7_styles() {
	wp_deregister_style( 'contact-form-7');
}
add_action( 'wp_print_styles', __NAMESPACE__ . '\\deregister_ct7_styles');
