<?php

use Roots\Sage\Setup;
use Timber\Timber;

$timber          = new Timber();
Timber::$dirname = 'views';

if ( ! class_exists( 'Timber' ) ) {

	add_action( 'admin_notices', function () {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
	} );

	return;
}

add_filter( 'get_twig', 'add_to_twig' );

function add_to_twig( $twig ) {
	$twig->addExtension( new Twig_Extension_StringLoader() );
	$twig->addFilter( new Twig_SimpleFilter( 'phone', 'cleanPhoneNumber' ) );
	$twig->addFilter( new Twig_SimpleFilter( 'digit_only', 'removeAllExceptDigit' ) );
	$twig->addFunction( new Twig_SimpleFunction( 'build_url', 'buildUrl' ) );

	return $twig;
}

/**
 * Timber
 */
class TwigSageTheme extends TimberSite {
	function __construct() {
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		parent::__construct();
	}

	function add_to_context( $context ) {

		/* Menu */
		$context['menu']        = new TimberMenu( 'primary_navigation' );
		$context['footer_menu'] = new TimberMenu( 'footer_navigation' );

		/* Site info */
		$context['site']           = $this;
		$context['options']        = get_fields( 'options' );
		$context['page_for_posts'] = get_permalink( get_option( 'page_for_posts' ) );

		/* Site info */
		$context['google_analytics_code'] = get_field( 'option_google_analytics_code', 'option' );
		$context['header_scripts']        = get_field( 'header_scripts', 'option' );
		$context['footer_scripts']        = get_field( 'footer_scripts', 'option' );

		/* Ajax init */
		$context['ajax_url']   = admin_url( 'admin-ajax.php' );
		$context['ajax_nonce'] = wp_create_nonce( 'itr_ajax_nonce' );

		/* Share buttons */
		if ( is_single() ) {
			global $post;
			$context['currentPost'] = new TimberPost( $post->ID );
		}

		return $context;
	}
}

new TwigSageTheme();

//Define cache length
if ( ! defined( 'CACHE_LENGTH' ) ) {
	define( 'CACHE_LENGTH', get_field( 'cache_length', 'option' ) );
}

//Restrict access
add_filter( 'acf/settings/show_admin', 'my_acf_show_admin' );
function my_acf_show_admin( $show ) {
	// provide a list of usernames who can edit custom field definitions here
	$admins = array(
		'Sumedia',
		'sumedia'
	);

	// get the current user
	$current_user = wp_get_current_user();

	return ( in_array( $current_user->user_login, $admins ) );
}


// Clean phone number
function cleanPhoneNumber( $number ) {
	$clean = '' . trim( str_replace( ' ', '', $number ) );
	if ( substr( $clean, 0, 1 ) == '+' ) {
		$clean = '' . substr_replace( $clean, '00', 0, 1 );
		$clean = str_replace( '(0)', '', $clean );
	}
	$clean = str_replace( array( '(', ')' ), '', $clean );

	return $clean;
}

function buildUrl( $current_url, $parameter = '', $value = '' ) {

	// Check if current url is passed, else generate current url
	if ( ! $current_url ) {
		$current_url = "http://$_SERVER[HTTP_HOST]"; // TODO: FIX HTTP OR HTTPS
	}

	$clean_url = strtok( $current_url, '?' );

	if ( empty( $parameter ) || empty( $value ) ) {
		$newUrl = $clean_url;
	} else {
		$newUrl = $clean_url . '?' . http_build_query( array_merge( $_GET, array( $parameter => $value ) ) );
	}

	return $newUrl;
}

function removeAllExceptDigit($string) {
    return preg_replace("/[^0-9]/", "", $string);
}

function my_wp_is_mobile() {
    static $is_mobile;

    if ( isset($is_mobile) )
        return $is_mobile;

    if ( empty($_SERVER['HTTP_USER_AGENT']) ) {
        $is_mobile = false;
    } elseif (
    strpos($_SERVER['HTTP_USER_AGENT'], 'Android') !== false
    || strpos($_SERVER['HTTP_USER_AGENT'], 'Silk/') !== false
    || strpos($_SERVER['HTTP_USER_AGENT'], 'Kindle') !== false
    || strpos($_SERVER['HTTP_USER_AGENT'], 'BlackBerry') !== false
    || strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mini') !== false ) {
        $is_mobile = true;
    } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'Mobile') !== false && strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') == false) {
        $is_mobile = true;
    } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') !== false) {
        $is_mobile = false;
    } else {
        $is_mobile = false;
    }

    return $is_mobile;
}
