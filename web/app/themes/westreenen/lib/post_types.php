<?php

class Sumedia_Functions {

	public function __construct() {

		add_action( 'admin_menu', array(
			$this,
			'remove_admin_menus'
		) );

		add_action( 'admin_init', array(
			$this,
			'remove_admin_submenus'
		) );

		add_action( 'wp_dashboard_setup', array(
			$this,
			'remove_dashboard_widgets'
		) );

		add_action( 'init', array(
			$this,
			'add_post_type'
		) );

		add_action( 'init', array(
			$this,
			'add_taxonomy'
		) );

		add_action( 'init', array(
			$this,
			'remove_comment_support'
		) );

		add_action( 'wp_before_admin_bar_render', array(
			$this,
			'admin_bar_render'
		) );

	}

	public function remove_comment_support() {
		remove_post_type_support( 'post', 'comments' );
		remove_post_type_support( 'page', 'comments' );
	}

	public function admin_bar_render() {
		global $wp_admin_bar;
		$wp_admin_bar->remove_menu( 'comments' );
		$wp_admin_bar->remove_menu( 'new-post' );
	}

	public function remove_admin_menus() {
		remove_menu_page( 'edit-comments.php' );
		remove_menu_page( 'link-manager.php' );
	}

	public function remove_dashboard_widgets() {
		global $wp_meta_boxes;
		unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press'] );
		unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments'] );
	}

	public function remove_admin_submenus() {

		global $submenu;

		//remove submenu items
		remove_submenu_page( 'themes.php', 'widgets.php' );
		remove_submenu_page( 'themes.php', 'theme-editor.php' );
		remove_submenu_page( 'plugins.php', 'plugin-editor.php' );
	}

	public function add_post_type() {

		// Kavels
		$this->setup_post_type(
            'kavels',
            'Kavels',
            'Kavel',
            'westreenen',
            'dashicons-admin-multisite',
            22,
            array(
    			'title',
    			'editor',
    			'thumbnail',
    			'custom-fields'
    		),
            false,
            array(),
            'page',
            true,
            true
        );

	}

	public function setup_post_type(
		$slug, $plural, $singular, $domain, $icon, $position = 20, $supports = array(
		'title',
		'editor',
		'thumbnail',
		'custom-fields'
	), $archive = true, $tax, $type, $hierachical, $public, $rewrite = ''
	) {

		$labels = array(
			'name'               => _x( $plural, $plural, $domain ),
			'singular_name'      => _x( $singular, $singular, $domain ),
			'menu_name'          => __( $plural, $domain ),
			'parent_item_colon'  => __( 'Parent Item:', $domain ),
			'all_items'          => __( 'All Items', $domain ),
			'view_item'          => __( 'View Item', $domain ),
			'add_new_item'       => __( 'Add New Item', $domain ),
			'add_new'            => __( 'Add New', $domain ),
			'edit_item'          => __( 'Edit Item', $domain ),
			'update_item'        => __( 'Update Item', $domain ),
			'search_items'       => __( 'Search Item', $domain ),
			'not_found'          => __( 'Not found', $domain ),
			'not_found_in_trash' => __( 'Not found in Trash', $domain ),
		);
		$args   = array(
			'label'               => __( $singular, $domain ),
			'description'         => __( $plural, $domain ),
			'labels'              => $labels,
			'supports'            => $supports,
			'hierarchical'        => $hierachical,
			'taxonomies'          => $tax,
			'public'              => $public,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => $position,
			'menu_icon'           => $icon,
			'can_export'          => true,
			'has_archive'         => $archive,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => $type,
		);

		if ( ! empty( $rewrite ) ) {
			$args['rewrite'] = array( 'slug' => $rewrite );
		}

		register_post_type( $slug, $args );

	}

	public function add_taxonomy() {
		// Taxonomy example
		$this->setup_taxonomy( 'taxonomy', 'Taxonomy', 'Taxonomies', 'teamred', array( 'posttypes' ), true, 'posttypes' );
	}

	public function setup_taxonomy( $tax, $plural, $singular, $domain, $connection, $public = true, $rewrite = '' ) {

		$labels = array(
			'name'                       => _x( $plural, $plural, $domain ),
			'singular_name'              => _x( $singular, $singular, $domain ),
			'menu_name'                  => __( $plural, $domain ),
			'all_items'                  => __( 'All Items', $domain ),
			'parent_item'                => __( 'Parent Item', $domain ),
			'parent_item_colon'          => __( 'Parent Item:', $domain ),
			'new_item_name'              => __( 'New Item Name', $domain ),
			'add_new_item'               => __( 'Add New Item', $domain ),
			'edit_item'                  => __( 'Edit Item', $domain ),
			'update_item'                => __( 'Update Item', $domain ),
			'separate_items_with_commas' => __( 'Separate items with commas', $domain ),
			'search_items'               => __( 'Search Items', $domain ),
			'add_or_remove_items'        => __( 'Add or remove items', $domain ),
			'choose_from_most_used'      => __( 'Choose from the most used items', $domain ),
			'not_found'                  => __( 'Not Found', $domain ),
		);
		$args   = array(
			'labels'            => $labels,
			'hierarchical'      => true,
			'public'            => true,
			'show_ui'           => true,
			'show_admin_column' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud'     => true,
		);

		if ( ! empty( $rewrite ) ) {
			$args['rewrite'] = array( 'slug' => $rewrite );
		}

		register_taxonomy( $tax, $connection, $args );

	}
}

// Instantiate the class
$Sumedia_Functions = new Sumedia_Functions();
