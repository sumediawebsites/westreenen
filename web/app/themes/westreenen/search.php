<?php
$context = Timber::get_context();

Timber::render('search.twig', $context, CACHE_LENGTH);
