<?php
$template              = '';
$context               = Timber::get_context();
$context['pagination'] = Timber::get_pagination();

if (is_post_type_archive('kavels')) {
    $template = 'kavels/archive.twig';
} else {
    $template = 'blog/archive.twig';
}

Timber::render( $template, $context, CACHE_LENGTH );
