/* ========================================================
 Check if mobile / desktop
======================================================== */

function deviceType() {
    //detect if desktop/mobile
    return window.getComputedStyle(document.querySelector('body'), '::before').getPropertyValue('content').replace(/"/g, "").replace(/'/g, "");
}

var MQ = deviceType();

/* ----------------------------------------------------------------
 * INIT ALL THE THINGS!
 * ---------------------------------------------------------------- */
(function ($) {
    // Use this variable to set up the common and page specific functions. If you
    // rename this variable, you will also need to rename the namespace below.
    var Sage = {
        // All pages
        'common': {
            init: function() {
                initMenu();

                if (MQ === 'mobile') {
                    scrollingHeader();
                }

                if ($('[data-letter]').length > 0) {
                    changeLetter();
                }

                if ($('.impression').length > 0) {
                    $('.impression-wrapper').click(function() {
                        $(this).toggleClass('flipped');
                    });
                }

                if ($('.scrolldown-icon').length > 0) {
                    scrollDown();
                }

                if ($('.slider').length > 0) {
                    playVideo();
                    initSlider();
                }

                if ($('.tabs').length > 0) {
                    switchTabs();
                }

                if ($('.kavel-slider').length > 0) {
                    kavelSlider();
                }

                if ($('.faq-holder').length > 0) {
                    initFaq();
                }

                if ($('.wpcf7').length > 0) {
                    $('.btn', $('.wpcf7')).after( '<span class="ajax-loader"></span>' );

                    if (MQ === 'mobile') {
                        openForm();
                    }
                }
            }
        }
    };

    // The routing fires all common scripts, followed by the page specific scripts.
    // Add additional events for more control over timing e.g. a finalize event
    var UTIL = {
        fire: function (func, funcname, args) {
            var fire;
            var namespace = Sage;
            funcname = (funcname === undefined) ? 'init' : funcname;
            fire = func !== '';
            fire = fire && namespace[func];
            fire = fire && typeof namespace[func][funcname] === 'function';

            if (fire) {
                namespace[func][funcname](args);
            }
        },
        loadEvents: function () {
            // Fire common init JS
            UTIL.fire('common');

            // Fire page-specific init JS, and then finalize JS
            $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function (i, classnm) {
                UTIL.fire(classnm);
                UTIL.fire(classnm, 'finalize');
            });

            // Fire common finalize JS
            UTIL.fire('common', 'finalize');
        }
    };

    // Load Events
    $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
