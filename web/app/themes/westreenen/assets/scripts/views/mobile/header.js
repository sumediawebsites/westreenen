var didScroll,
    lastScrollTop = 0,
    delta = 5,
    navbarHeight = $('.site-header').outerHeight();

function hasScrolled() {

    var st = $(this).scrollTop();

    // Make sure they scroll more than delta
    if (Math.abs(lastScrollTop - st) <= delta) {
        return;
    }

    if (st > lastScrollTop && st > navbarHeight) {
        $('.site-header').addClass('move-up');
    } else {
        if (st + $(window).height() < $(document).height()) {
            $('.site-header').removeClass('move-up');
        }
    }

    lastScrollTop = st;

}

function scrollingHeader() {

    $(window).scroll(function(event) {
        didScroll = true;
    });

    setInterval(function() {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    }, 250);

}
