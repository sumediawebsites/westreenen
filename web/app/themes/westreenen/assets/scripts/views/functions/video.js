function playVideo(el) {

    var video_url,
        container,
        content,
        clone,
        positionTop,
        positionLeft,
        totalposition,
        widthcontainer,
        heightcontainer,
        scaleWidth,
        scaleHeight,
        widowWidthCheck,
        originalThis;

    if (typeof(el) === 'undefined' || el === '') {
        el = $('.video-play-modal');
    }

    el.click(function() {
        originalThis = $(this);
        video_url = originalThis.attr('href');
        container = originalThis.parents('.video-container');
        clone = $('.videoclone');

        positionTop = $(container).offset().top - $(window).scrollTop();
        positionLeft = $(container).offset().left - $(window).scrollLeft();

        widthcontainer = container.outerWidth();
        heightcontainer = container.outerHeight();

        scaleWidth = widthcontainer * 1.9;
        scaleHeight = heightcontainer * 1.9;

        widowWidthCheck = $(window).width() * 0.9;

        if(scaleWidth > widowWidthCheck) {
            scaleWidth = widowWidthCheck;
            scaleHeight= (widowWidthCheck / 16) * 9;
        } else {
            scaleWidth = scaleWidth;
            scaleHeight = scaleHeight;
        }

        setTimeout(function() {
            $(clone).width(widthcontainer).height(heightcontainer);
            $(clone).offset({top: positionTop, left: positionLeft});
            $(container).clone().attr('class', 'video-container').appendTo(".videoclone");
            $(clone).find('.video-icon.video-play-modal').css('visibility', 'hidden');
            $(clone).fadeIn();
            $(container).css('visibility', 'hidden');

            $('body').append('<div class="videoclone-background"></div>');
        }, 100);

        setTimeout(function() {
            $('.videoclone-background').fadeIn();
            $(clone).addClass('centervideo');
            $(clone).width(scaleWidth).height(scaleHeight);
            $(clone).find('.video-container').append('<video id="video-modal" class="embed-responsive-item" width="100%" height="100%" autoplay playsinline muted><source src="'+ video_url +'" type="video/mp4"></video>');

            setTimeout(function() {
                $(clone).find('picture').fadeOut('slow');
                $(clone).find('.video-container').prepend('<a class="close">Sluiten</a>');
                setTimeout(function() {
                    $(clone).find('.video-container .close').addClass('closein');
                }, 650);
            }, 650);
        }, 200);

        $(document).on('click','.videoclone-background, .close', function() {
            $(originalThis).parents('.video-container').css('visibility', 'visible');
            $(clone).fadeOut();
            $(clone).empty();
            $(clone).removeClass('centervideo');
            $(clone).css({'top':'','left':''});
            $('.videoclone-background').fadeOut();
            $('.videoclone-background').remove();
        });

        return false;
    });


}
