function initFaq() {

    $('.faq__item__question').on('click', function() {
        var expand = $(this).next('.faq__item__answer');

        $('.faq__item__question').not($(this)).removeClass('active');
        $('.faq__item__answer').not(expand).stop(true, false).slideUp(400);
        $(this).toggleClass('active');
        expand.stop(true, false).slideToggle(400);

        return false;
    });

}
