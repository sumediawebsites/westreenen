function switchTabs() {

    $(document).on('click','.tab', function() {

        $(this).addClass('active').siblings('[data-tab]').removeClass('active');
        $(this).parents('.site-container__inner').find("[data-content='" + $(this).data('tab') + "']").addClass('active').siblings().removeClass('active');

    });

}
