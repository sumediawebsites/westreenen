function scrollDown() {

    $(document).on('click', '.scrolldown-icon', function () {
        $('html, body').animate({
            scrollTop: $(this).parents('.slider-wrapper-outer').next().offset().top
        }, 1200);
    });

}
