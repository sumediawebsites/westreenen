/* ========================================================
 Slider
======================================================== */

function muteVideo() {

    $(document).on('click', '.audio-button', function () {

        if ($("#video").prop('muted')) {
            $("#video").prop('muted', false);
        } else {
            $("#video").prop('muted', true);
        }

        $(this).toggleClass('mutevideo');

    });

}

function sliderPauseVideo() {

    $(document).on('click', '.slider-wrapper', function () {
        var $this = $(this);
        $this.find('#video').each(function () {
            if (this.paused) {
                this.play();
                $('.slider-wrapper').removeClass('video-paused');
            } else {
                this.pause();
                $('.slider-wrapper').addClass('video-paused');
            }
        });
    });

}

function sliderCreateVideo(slider) {

    var video = $('.slider__video-play-modal', '.slick-active');

    $('.slider__item img').css('opacity', '1');
    $('.slider-wrapper').removeClass('video-playing video-paused');
    $('.slider-wrapper-outer').removeClass('video-playing');
    $('#video').remove();


    if (video.length > 0) {
        var video_url = video.attr('href');
        if ($('#video').length === 0) {
            $('.slick-active', slider).find('.image-holder').append('<video id="video" class width="100%" height="100%" muted autoplay playsinline><source src="' + video_url + '" type="video/mp4"></video>');

            $('.slick-active', slider).find('img').animate({
                'opacity': 0
            }, {
                duration: 800
            });

            $('#video').animate({
                'opacity': 1
            }, {
                duration: 800
            });

            slider.slick('slickPause');

            document.getElementById('video').addEventListener('ended', function () {
                slider.slick('slickPlay');
            }, false);
        }
    }

    $(window).bind('scroll', function () {
        if (!$('.slider-wrapper').hasClass('video-paused')) {
            var winHeight = $(window).height() / 2;

            if ($(window).scrollTop() > winHeight) {
                $('.slick-active video').each(function () {
                    this.pause();
                });
            } else {
                $('.slick-active video').each(function () {
                    this.play();
                });
            }
        }
    });

    $(video).parents('.slider-wrapper').addClass('video-playing');
    $(video).parents('.slider-wrapper-outer').addClass('video-playing');

}

function initSlider() {

    var slider = $('.slider'),
        autoplay = true;

    slider.on('init', function (slick) {
        sliderCreateVideo(slider);

        if ($('.slick-slide', slider).length === 1) {
            $('.slick-dots').hide();
        }
    });

    if (slider_speed === false) {
        autoplay = false;
    }

    var showarrows = false,
        TheNextArrow = '',
        ThePrevArrow = '';

    if ($('.sliderarrows').length > 0) {
        showarrows = true;
        TheNextArrow = $('.next-slide-arrow');
        ThePrevArrow = $('.prev-slide-arrow');
    } else {
        showarrows = false;
        TheNextArrow = '<button type="button" class="slick-next">Next</button>';
        ThePrevArrow = '<button type="button" class="slick-prev">Previous</button>';
    }

    slider.slick({
        autoplay: autoplay,
        autoplaySpeed: slider_speed,
        infinite: true,
        arrows: showarrows,
        nextArrow: TheNextArrow,
        prevArrow: ThePrevArrow,
        dots: false,
        mobileFirst: true,
        adaptiveHeight: false,
        pauseOnHover: false
    });

    if (MQ === 'desktop') {
        slider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
            $('video', slider).each(function () {
                this.pause();
            });
        });
    }

    slider.on('afterChange', function (slick, currentSlide) {
        sliderCreateVideo(slider);
    });

    sliderPauseVideo();
    muteVideo();

}

function kavelSlider() {

    var showarrows = false,
        TheNextArrow = '',
        ThePrevArrow = '',
        kavelStart = '1';

    if ($('.kavelarrows').length > 0) {
        showarrows = true;
        TheNextArrow = $('.next-kavel-arrow');
        ThePrevArrow = $('.prev-kavel-arrow');
    } else {
        showarrows = false;
        TheNextArrow = '<button type="button" class="slick-next">Next</button>';
        ThePrevArrow = '<button type="button" class="slick-prev">Previous</button>';
    }

    kavelStart = $('.kavel-slider').data('start') - 1;

    $('.kavel-slider').slick({
        autoplay: false,
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: false,
        initialSlide: kavelStart,
        arrows: showarrows,
        nextArrow: TheNextArrow,
        prevArrow: ThePrevArrow,
        dots: false,
        responsive: [
            {
                breakpoint: 1921,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

}
