function changeLetter() {

    $('[data-letter]').on('mouseenter', 'a', function(ev) {
        var letter = $(this).parent().data('letter');

        $('.letter__appended').removeClass('animate animate-out');
        $('<span class="letter__appended">' + letter + '</span>').appendTo('.letter').delay(50).queue(function(next) {
            $(this).addClass('animate');
            next();
        });
        $('.letter__default').removeClass('animate').addClass('animate-out');

    }).mouseleave(function() {

        $('.letter__default').removeClass('animate-out').addClass('animate');
        $('.letter .letter__appended').removeClass('animate').addClass('animate-out').delay(50).queue(function(next) {
            $('.letter span:not(.letter__default)').not($('.letter .letter__appended').last()).remove();
            next();
        });

    });

}
