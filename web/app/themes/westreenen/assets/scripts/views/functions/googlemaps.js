function googleMaps(element) {
    var map, el = $(element), lat = el.attr('data-lat'), lng = el.attr('data-lng'), location = new google.maps.LatLng(lat, lng), drag;
    if ($(window).innerWidth() > 767) {
        drag = true;
    } else {
        drag = false;
    }
    function initialize() {
        var mapOptions = {
            zoom: 15,
            center: location,
            scrollwheel: false,
            draggable: drag,
            disableDefaultUI: true,
            mapTypeControlOptions: {mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']}
        };
        map = new google.maps.Map(document.getElementById(element.replace('#', '')), mapOptions);
        var marker = new google.maps.Marker({position: new google.maps.LatLng(lat, lng), map: map});
    }

    initialize();
    var getCen = map.getCenter();
    google.maps.event.addDomListener(window, 'resize', function () {
        map.setCenter(getCen);
    });
}