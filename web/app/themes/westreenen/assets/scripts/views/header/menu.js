function openMenu() {

    $('.hamburger').on('click', function(ev) {
        ev.preventDefault();

        $('html').toggleClass('menu-open');
    });

}

function closeMenu() {

    $(document).on('click', function(e) {
        var container = $('.site-header');
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            $('html').removeClass('menu-open');
        }
    });

    $(document).keyup(function(e) {
        if (e.keyCode === 27) {
            $('html').removeClass('menu-open');
        }
    });

}

function initMenu() {
    openMenu();
    closeMenu();
}
